package Scenario3;

/**
 * Created by Maarten on 12/16/2016.
 */
public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    @Override
    public void unload() {

    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}