package Scenario3;

/**
 * Created by Maarten on 12/16/2016.
 */
public class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }

    public void unload(){
        if(realImage != null){
            realImage = null;
            System.out.printf("Unloading %s from memory\n", fileName );
        }
    }
}