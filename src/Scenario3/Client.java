package Scenario3;

/**
 * Created by Maarten on 12/16/2016.
 */
public class Client {
    public static void main(String[] args) {
        Image image1 = new ProxyImage("image1");
        Image image2 = new ProxyImage("image2");
        Image image3 = new ProxyImage("image3");
        Image image4 = new ProxyImage("image4");
        Image image5 = new ProxyImage("image5");

        image1.display();
        image2.display();
        image3.display();
        image4.display();
        image5.display();
        System.out.println("");
        image1.display();
        image2.display();
        image3.display();
        image4.display();
        image5.display();
        image1.unload();
        image1.display();
    }
}
