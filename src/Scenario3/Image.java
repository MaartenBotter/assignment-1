package Scenario3;

/**
 * Created by Maarten on 12/16/2016.
 */
public interface Image {
    void display();
    void unload();
}
