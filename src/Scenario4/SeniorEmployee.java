package Scenario4;

public class SeniorEmployee extends Employee{
    public SeniorEmployee(String name){
        super.name = name;
    }


    @Override
    public String getLevel() {
        return "Senior";
    }

    @Override
    public void raiseSalary() {
        super.setSalary(super.getPay()*1.07);
        super.raiseSalary();
    }

    @Override
    public String toString(){
        String r = "Senior " + super.toString();
        return r;
    }
}
