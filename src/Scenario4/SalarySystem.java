package Scenario4;

import java.util.ArrayList;


public class SalarySystem {
	
	private ArrayList empArray = new ArrayList();
	
	public void addColleague(Employee c) {
		empArray.add(c);
	}
	
	public Employee search(String s) {
		for (int i = 0; i < empArray.size(); i++)
			if (((Employee)empArray.get(i)).getName().equals(s)) return (Employee)empArray.get(i);
		//return new Employee("no employee has been found",-1);
		return null;
	}
	
	public SalarySystem() {
		Employee j = EmployeeFactory.createEmployee("Jaap",0);
		Employee m = EmployeeFactory.createEmployee("Fred",1);
		Employee s = EmployeeFactory.createEmployee("Hans",2);
		j.setSalary(1700.0);
		m.setSalary(2000);
		s.setSalary(2300);
		addColleague(j);
		addColleague(m);
		addColleague(s);
	}
	
	public void printAll() {
		for (Object e : empArray) {
			System.out.print(e.toString() + "\n");
		}
	}
}


