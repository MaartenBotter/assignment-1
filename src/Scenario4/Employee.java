package Scenario4;

public abstract class Employee {
	protected String name;
	private double sal;
	
	public Employee() {
		name = "";
		sal = 0;
	}
	
	public Employee(String n) {
		name = n;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract String getLevel();
	
	public void setSalary(double s) {
		sal = s;
	}
	
	public double getPay() {
		return sal;
	}

	public void raiseSalary(){
		System.out.println("Log message: employee " + name + " has new salary: " + sal);
	}

	public String toString() {
		String r="";
		r += "employee " + name + " has salary " + sal;
		return r;
	}

}
