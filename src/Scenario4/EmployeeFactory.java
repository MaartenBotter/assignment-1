package Scenario4;

public class EmployeeFactory {

    public static Employee createEmployee(String name, int code) {

        Employee emp = null;
        if (code == 0) {
            emp = new JuniorEmployee(name);
        } else if (code == 1) {
            emp = new MediorEmployee(name);
        } else if (code == 2) {
            emp = new SeniorEmployee(name);
        }

        return emp;
    }
}