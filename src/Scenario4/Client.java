package Scenario4;

/**
 * Created by Maarten on 12/18/2016.
 */
public class Client {
    public static void main(String args[]) {
        // raise salary of all employees
        SalarySystem s = new SalarySystem();
        s.printAll();
        Employee e = s.search("Jaap");
        e.raiseSalary();
        e = s.search("Fred");
        e.raiseSalary();
        e = s.search("Hans");
        e.raiseSalary();
        s.printAll();
    }
}
