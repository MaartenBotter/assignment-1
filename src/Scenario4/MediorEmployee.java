package Scenario4;

public class MediorEmployee extends Employee{
    public MediorEmployee(String name){
        super.name = name;
    }

    @Override
    public String getLevel() {
        return "Medior";
    }

    @Override
    public void raiseSalary() {
        super.setSalary((super.getPay()*1.06)+100);
        super.raiseSalary();
    }

    @Override
    public String toString(){
        String r = "Normal " + super.toString();
        return r;
    }
}
