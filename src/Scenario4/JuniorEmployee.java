package Scenario4;

public class JuniorEmployee extends Employee{

    public JuniorEmployee(String name){
        super.name = name;
    }

    @Override
    public String getLevel() {
        return "Junior";
    }

    @Override
    public void raiseSalary() {
        super.setSalary((super.getPay()*1.05)+100);
        super.raiseSalary();
    }

    @Override
    public String toString(){
        String r = "Junior " + super.toString();
        return r;
    }
}
