package Scenario1;import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Maarten on 12/15/2016.
 */
public class SaveFile {
    private allTypes type;

    public void Save(){
        System.out.println("Voer de bestandsnaam in van het op te slaan bestand:");
        Scanner in = new Scanner(System.in);
        String filename = in.nextLine();
        System.out.println("Kies een van de volgende bestandstypen:");
        ArrayList<allTypes> formatlist = new ArrayList<>();
        for (allTypes a : allTypes.values()) {
            formatlist.add(a);
        }
        for (int j = 0; j < formatlist.size(); j++) {
            System.out.println((j + 1) + "\t" + formatlist.get(j).Full());
        }
        String choice = in.nextLine();
        Integer number = Integer.parseInt(choice);
        number -= 1;
        this.type = formatlist.get(number);
        System.out.println("Bestand opgeslagen als " + filename + type.Abbr());
        }
    }
