package Scenario1; /**
 * Created by Maarten on 12/15/2016.
 */
public enum allTypes {
    WORD97("Word97",".doc"),
    XML("Word-XML",".xml"),
    RTF("Rtf",".rtf"),
    HTML("Html",".html"),
    PLAINTEXT("Plaintext",".html");

    private final Object[] values;

    allTypes(Object... vals) {
        values = vals;
    }

    public String Full(){
        return (String) values[0];
    }

    public String Abbr(){
        return (String) values[1];
    }
}
