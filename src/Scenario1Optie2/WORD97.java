package Scenario1Optie2;

/**
 * Created by Maarten on 12/16/2016.
 */
public class WORD97 implements SaveType{

    private String name = "Word97";
    private String suffix = ".doc";

    @Override
    public String opslaan(String bestandsnaam) {
        return "Bestand opgeslagen als " + bestandsnaam + suffix;
    }

    @Override
    public String getName() {
        return name;
    }
}
