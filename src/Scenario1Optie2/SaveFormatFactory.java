package Scenario1Optie2;

import java.util.ArrayList;

/**
 * Created by Maarten on 12/16/2016.
 */
public class SaveFormatFactory {
    private ArrayList<SaveType> list = new ArrayList<>();

    protected SaveFormatFactory(){
        SaveType Word = new WORD97();
        SaveType XML = new XML();
        SaveType Rtf = new Rtf();
        SaveType Html = new Html();
        SaveType Plaintext = new Plaintext();
        list.add(Word);
        list.add(XML);
        list.add(Rtf);
        list.add(Html);
        list.add(Plaintext);
    }

    protected String printList(){
        String s = "";
        int i = 1;
        for(SaveType st:list){
            s = s + i + "\t" + st.getName() + "\n";
            i++;
        }
        return s;
    }

    protected String saveTheFile(int number, String bestandsnaam){
        return list.get((number-1)).opslaan(bestandsnaam);
    }
}
