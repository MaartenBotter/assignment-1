package Scenario1Optie2;

/**
 * Created by Maarten on 12/16/2016.
 */
public class XML implements SaveType{

    private String name = "XML";
    private String suffix = ".xml";

    @Override
    public String opslaan(String bestandsnaam) {
        return "Bestand opgeslagen als " + bestandsnaam + suffix;
    }

    @Override
    public String getName() {
        return name;
    }
}
