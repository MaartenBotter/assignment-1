package Scenario1Optie2;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Maarten on 12/16/2016.
 */
public class Client {
    public static void main(String[] args) {
        SaveFormatFactory factory = new SaveFormatFactory();
        //Getting filename
        System.out.println("Voer de bestandsnaam in van het op te slaan bestand:");
        Scanner in = new Scanner(System.in);
        String filename = in.nextLine();
        //Printing all types from list in factory
        System.out.println("Kies een van de volgende bestandstypen:");
        System.out.println(factory.printList());
        //Getting number associated with type
        String typenumber = in.nextLine();
        Integer number = Integer.parseInt(typenumber);
        String saved = factory.saveTheFile(number,filename);
        System.out.println(saved);
    }
}
