package Scenario1Optie2;

/**
 * Created by Maarten on 12/16/2016.
 */
public class Html implements SaveType{

    private String name = "Html";
    private String suffix = ".html";

    @Override
    public String opslaan(String bestandsnaam) {
        return "Bestand opgeslagen als " + bestandsnaam + suffix;
    }

    @Override
    public String getName() {
        return name;
    }
}
