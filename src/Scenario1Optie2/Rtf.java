package Scenario1Optie2;

/**
 * Created by Maarten on 12/16/2016.
 */
public class Rtf implements SaveType{

    private String name = "Rtf";
    private String suffix = ".rtf";

    @Override
    public String opslaan(String bestandsnaam) {
        return "Bestand opgeslagen als " + bestandsnaam + suffix;
    }

    @Override
    public String getName() {
        return name;
    }
}
