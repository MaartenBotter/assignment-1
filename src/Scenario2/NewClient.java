package Scenario2;

/**
 * Created by Maarten on 12/15/2016.
 */
public class NewClient {
    private LegacyApplication adaptee = new LegacyApplication();
    public static void drawRectangle(int x1, int y1, int x2, int y2){
        LegacyApplication.Display((Math.min(x1,x2)), Math.max(y1,y2),Math.abs(x2-x1), Math.abs(y2-y1));
    }
}
