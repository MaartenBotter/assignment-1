package Scenario2;

import java.util.Scanner;

/**
 * Created by Maarten on 12/15/2016.
 */
public class Main {
    public static void main(String[] args) {
        LegacyApplication app = new LegacyApplication();
        NewClient client = new NewClient();
        LegacyApplication.Display(2,2,5,3);
        NewClient.drawRectangle(2,-1,7,2);
        System.out.println("LEGACY\nEnter bottom left x coordinate");
        Scanner in = new Scanner(System.in);
        int botleftx = Integer.parseInt(in.nextLine());
        System.out.println("Enter bottom left y coordinate");
        int botlefty = Integer.parseInt(in.nextLine());
        System.out.println("Enter width");
        int width = Integer.parseInt(in.nextLine());
        System.out.println("Enter height");
        int height = Integer.parseInt(in.nextLine());
        LegacyApplication.Display(botleftx,botlefty,width,height);
        System.out.println("NEW\nEnter top left x coordinate");
        int topx = Integer.parseInt(in.nextLine());
        System.out.println("Enter top left y coordinate");
        int topy = Integer.parseInt(in.nextLine());
        System.out.println("Enter bottom right x coordinate");
        int botx = Integer.parseInt(in.nextLine());
        System.out.println("Enter bottom right y coordinate");
        int boty = Integer.parseInt(in.nextLine());
        NewClient.drawRectangle(topx,topy,botx,boty);
    }
}
